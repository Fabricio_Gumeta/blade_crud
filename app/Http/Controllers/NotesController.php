<?php

namespace App\Http\Controllers;

use App\Notes;
use Illuminate\Http\Request;
use App;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notas = App\Notes::paginate(3);
        return view('notes.inicio',compact('notas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notaAgregar = new Notes;
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);
        $notaAgregar->nombre = $request->nombre;
        $notaAgregar->descripcion = $request->descripcion;
        $notaAgregar->save();
        return back()->with('agregar','La nota fue agregada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function show(Notes $notes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notaEditar = App\Notes::findOrFail($id);
        return view('notes.editar',compact('notaEditar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notaUpdate =App\Notes::findOrFail($id);
        $notaUpdate->nombre = $request->nombre;
        $notaUpdate->descripcion = $request->descripcion;
        $notaUpdate->save();
        return back()->with('update','La nota fue editada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notes  $notes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notaDelete = App\Notes::findOrFail($id);
        $notaDelete->delete();
        return back()->with('delete', 'La nota fue eliminada correctamente');
    }
}
