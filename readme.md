Comandos:
Iniciar servidor con el comando: php artisan serve
Generar test con el comando: php artisan make:test NombreDeLaPruebaTest
Iniciar test con el comando: vendor/bin/phpunit
Generar un nuevo controlador: php artisan make:controller UserController
Para utilizar Blade debemos llamar anuestro archivo con la extención .blade.php
Crear un modelo: php artisan make:model Notes -cmr
Migrar DB: php artisan migrate