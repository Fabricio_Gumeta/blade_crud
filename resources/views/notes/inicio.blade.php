@extends('plantilla')

@section('title')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>

                @foreach ($notas as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>
                            <a href="{{route('editar', $item->id)}}" class="btn btn-warning">Editar</a>
                            <form action="{{route('eliminar', $item->id)}}" method="POST" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>

            @if (session('delete'))
            <div class="alert alert-success mt-3">
                {{session('delete')}}
            </div>
        @endif

            {{$notas->links()}}
        </div>
        {{-- Tabla del formulario --}}
        <div class="col-md-5">
            <h3 class="text-center mb-4">Agregar Notas</h3>

            <form action="{{route('store')}}" method="POST">
                @csrf

                <div class="form-group">
                    <input type="text" name="nombre" id="nombre" class="form-control"  placeholder="Nombre de la nota">
                </div>

                @error('nombre')
                    <div class="alert alert-danger">
                        El nombre es obligatorio
                    </div>
                @enderror

                <div class="form-group">
                    <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Descripcion de la nota">
                </div>

                @error('descripcion')
                    <div class="alert alert-danger">
                        La descripcion es obligatoria
                    </div>
                @enderror

                <button type="submit" class="btn btn-success btn-block">Agregar Nota</button>
            </form>

            @if (session('agregar'))
                <div class="alert alert-success mt-3">
                    {{session('agregar')}}
                </div>
            @endif
        </div>
        {{-- Fin Tabla formulario --}}
    </div>
@endsection