@extends('plantilla')

@section('title')

@section('content')
    <h3 class="text-center mb-3 pt-3">Editar nota {{$notaEditar->id}}</h3>

    <form action="{{route('update', $notaEditar->id)}}" method="POST">
        @method('PUT')
        @csrf

        <div class="form-group">
            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$notaEditar->nombre}}">
        </div>

        <div class="form-group">
            <input type="text" name="descripcion" id="descripcion" class="form-control" value="{{$notaEditar->descripcion}}">
        </div>
        <button type="submit" class="btn btn-warning btn-block">Editar Nota</button>
    </form>

    @if (session('update'))
    <div class="alert alert-success mt-3">
        {{session('update')}}
    </div>
@endif

@endsection